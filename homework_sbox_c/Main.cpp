#include <iostream>
#include <string>


class Animal
{

public:
	Animal() {}
	Animal(std::string _animalVoise) : animalVoice(_animalVoise) {}

	std::string animalVoice;

	virtual void Voice()
	{
		std::cout << animalVoice;
	}
};
class Dog : public Animal
{
	std::string dogVoice = "Woof! ";
public:
	Dog() {}

	void Voice() override
	{
		std::cout << "Dog: " << dogVoice; 
	}
};

class Cat : public Animal
{
	std::string catVoice = "Mya! ";
public:
	Cat() {}

	void Voice() override
	{
		std::cout << "Cat: " << catVoice;
	}
};

class Sheep : public Animal
{
	std::string sheepVoice = "B-e-e! ";
public:
	Sheep() {}

	void Voice() override
	{
		std::cout << "Sheep: " << sheepVoice;
	}
};

int main()
{
	Animal* p[3]{ new Dog(), new Cat(), new Sheep() };

	for (int i = 0; i < 3; i++)
	{
		p[i]->Voice();

	}

}


